package testClassPackage_testNG;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.Test;

import commonMethodPackage_testNG.Trigger_API_Put_testNG;
import commonUtilityPackage_testNG.Handle_API_logs_testNG;
import io.restassured.path.json.JsonPath;

public class Put_tc1_testNG extends Trigger_API_Put_testNG {
	@Test
	public static void executor() throws IOException {
		String Requestbody=Put_request_TestNG();

		File directoryname = Handle_API_logs_testNG.create_log_directory("Put_tc1_testNG");

		for (int i = 0; i < 5; i++) {
			int statuscode = Trigger_API_Put_testNG.extract_put_statuscode_testNG(Requestbody, Put_endpoint_test());
			System.out.println("The Statuc code here in put_tc is:" + statuscode);

			if (statuscode == 200) {

				String Responsebody = Trigger_API_Put_testNG.extract_put_response_testNG(Requestbody, Put_endpoint_test());
				System.out.println(Responsebody);
				Handle_API_logs_testNG.evidence_creator(directoryname, "Put_tc1_testNG", Put_endpoint_test(), Put_request_TestNG(), Responsebody);
				validator(Requestbody,Responsebody);
				break;
			} else {
				System.out.println("Retrying as statuc code was invalid");
			}
		}

	}

	public static void validator(String Requestbody , String Responsebody) throws IOException {

		JsonPath jsp_res = new JsonPath(Responsebody);
		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");
		String res_updatedAt = jsp_res.getString("updatedAt");
		res_updatedAt = res_updatedAt.substring(0, 10);

		LocalDateTime d = LocalDateTime.now();
		String Date = d.toString().substring(0, 10);

		JsonPath jsp_req = new JsonPath(Put_request_TestNG());
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(Date, res_updatedAt);

	}
}