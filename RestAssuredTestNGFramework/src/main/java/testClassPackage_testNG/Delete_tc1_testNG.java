package testClassPackage_testNG;

import java.io.File;
import java.io.IOException;

import org.testng.annotations.Test;

import commonMethodPackage_testNG.Trigger_Delete_API_testNG;
import commonUtilityPackage_testNG.Handle_API_logs_testNG;

public class Delete_tc1_testNG extends Trigger_Delete_API_testNG{
	@Test
	public static void executor() throws IOException {
		
		File directoryname=Handle_API_logs_testNG.create_log_directory("Delete_tc1_testNG");
		
		int statuscode= extract_delete_statuscode(Delete_Endpoint_test());
		System.out.println("Testclass statuscode:" + statuscode);
		Handle_API_logs_testNG.evidence_creator(directoryname, "Delete_tc1_testNG", Delete_Endpoint_test(), null, null);
	}

}
