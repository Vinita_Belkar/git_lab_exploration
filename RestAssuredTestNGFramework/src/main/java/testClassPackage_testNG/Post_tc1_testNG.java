package testClassPackage_testNG;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.Test;

//import commonMethodPackage.Trigger_API_PostMethod;
import commonMethodPackage_testNG.Trigger_Post_API_testNg;
import commonUtilityPackage_testNG.Handle_API_logs_testNG;
//import commonUtilitypackage.Handle_API_LOGS;
import io.restassured.path.json.JsonPath;

public class Post_tc1_testNG extends  Trigger_Post_API_testNg {
	@Test
	public static void executor() throws IOException {
		String Requestbody=Post_request_testNG();

		//the first method of any Test class should be to create the log directory
		//the test case class name we are executing will pass it as arguement.
		File dirname = Handle_API_logs_testNG.create_log_directory("Post_tc1_testNG");
		//int statuscode = 0;

		for (int i = 0; i < 5; i++) {
			int statuscode =  Extract_API_post_statuscode_testNG(Requestbody, post_endpoint_test());
			//System.out.println(statuscode);

			if (statuscode == 201) {
				String responseBody = Trigger_Post_API_testNg.Extract_API_POST_RESPONSE_TESTNG(Requestbody,  post_endpoint_test());
				//System.out.println(responseBody);
				 Handle_API_logs_testNG.evidence_creator(dirname,"Post_tc1_testNG", post_endpoint_test(),Requestbody,responseBody);
				validator(Requestbody, responseBody);
				break;

			} else {
				System.out.println("Desired status code not found hence Retrying");
			}
		}
	}

	public static void validator(String Requestbody,String responseBody) throws IOException {

		// Creating object of JsonPath and parse the Response body
		JsonPath jsp = new JsonPath(responseBody);
		String res_name = jsp.getString("name");
		//System.out.println(res_name);

		String res_job = jsp.getString("job");
		//System.out.println(res_job);

		String res_id = jsp.getString("id");
		//System.out.println("id");

		String res_createdAt = jsp.getString("createdAt");
		//System.out.println(res_createdAt);

		String currentDate = res_createdAt.substring(0, 10);
		//System.out.println(currentDate);

		//Generating the system current date
		LocalDateTime D1 = LocalDateTime.now();
		String date = D1.toString().substring(0,10);
		System.out.println(date);

		// create JsonPath and parse the requestBody
		JsonPath jsp1 = new JsonPath(Requestbody);
		String req_name = jsp.getString("name");
		//System.out.println(req_name);

		String req_job = jsp.getString("job");
		//System.out.println(req_job);

		// Validating the response body parameter
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(currentDate, date);
		Assert.assertNotNull(res_id);
	}

}

