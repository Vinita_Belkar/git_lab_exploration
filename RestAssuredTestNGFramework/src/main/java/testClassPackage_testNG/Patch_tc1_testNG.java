package testClassPackage_testNG;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.Test;

import commonMethodPackage_testNG.Trigger_Patch_API_testNG;
import commonUtilityPackage_testNG.Handle_API_logs_testNG;
import io.restassured.path.json.JsonPath;

public class Patch_tc1_testNG extends Trigger_Patch_API_testNG{
	@Test
	public static void executor() throws IOException {
		String Requestbody= Patch_request_testNG();
		
		File Directoryname=Handle_API_logs_testNG.create_log_directory("Patch_tc1_testNG");
		
		for(int i =0; i<5; i++) {
			int statuscode= Trigger_Patch_API_testNG.extract_patch_statuscode_testNG(Requestbody, Patch_endpoint_test());
			System.out.println(statuscode);
			
			if(statuscode==200) {
			String Responsebody=Trigger_Patch_API_testNG.extract_patch_response_testNG(Requestbody, Patch_endpoint_test());
			System.out.println("responsebody of patch api is:" + Responsebody);
			Handle_API_logs_testNG.evidence_creator(Directoryname, "Patch_tc1_testNG",Patch_endpoint_test(), Patch_request_testNG(),Responsebody);
			validator(Requestbody,Responsebody);
			break;
		}
			else {
				System.out.println("Desired status nit found hence retrying");
			}
		}	
		
	}

	public static void validator(String Requestbody,String Responsebody ) throws IOException {
		
		JsonPath jsp_res=new JsonPath(Responsebody);
		String res_name=jsp_res.getString("name");
		String res_job=jsp_res.getString("job");
		String res_updatedAt=jsp_res.getString("updatedAt");
		res_updatedAt=res_updatedAt.substring(0, 10);
		
		LocalDateTime L=LocalDateTime.now();
		String systemDate=L.toString().substring(0, 10);
		
		JsonPath jsp_req=new JsonPath(Requestbody);
		String req_name=jsp_req.getString("name");
		String req_job=jsp_req.getString("job");
		
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_updatedAt, systemDate);
		
	}
}
