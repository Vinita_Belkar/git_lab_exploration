package request_repository_testNG;

import java.io.IOException;
import java.util.ArrayList;

import commonUtilityPackage_testNG.Excel_data_reader_testNG;


public class Post_request_repository_testNg extends Endpoints_testNg {
	
	
	public static String Post_request_testNG() throws IOException {

		ArrayList<String> Excel_data=Excel_data_reader_testNG.Read_Excel_Data("API_TestNG.xlsx", "PostAPI_TestNG", "Post_TC2");
		//System.out.println(Excel_data);
		String req_name=Excel_data.get(1);
		String req_job=Excel_data.get(2);

		String Requestbody="{\r\n"
				+ "    \"name\": \""+ req_name +"\",\r\n"
				+ "    \"job\": \"" + req_job + "\"\r\n"
				+ "}";
		return Requestbody;
	}

}
