package request_repository_testNG;

import java.io.IOException;
import java.util.ArrayList;

import commonUtilityPackage_testNG.Excel_data_reader_testNG;

public class Put_request_repository_testNG extends Endpoints_testNg{
	
	public static String Put_request_TestNG() throws IOException {
		
		ArrayList<String> excel_data_testNG_put=Excel_data_reader_testNG.Read_Excel_Data("API_TestNG.xlsx", "PutAPI_TestNG", "Put_tc2_testNG");
		System.out.println(excel_data_testNG_put);
		String req_name=excel_data_testNG_put.get(1);
		String req_job=excel_data_testNG_put.get(2);
		
		String RequestBody="{\r\n"
				+ "    \"name\": \""+req_name+"\",\r\n"
				+ "    \"job\": \""+req_job+"\"\r\n"
				+ "}";
		System.out.println(RequestBody);
		return RequestBody;
		
		
	}

}
