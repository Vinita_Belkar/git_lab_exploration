package request_repository_testNG;

public class Endpoints_testNg {
	
	static String hostname = "https://reqres.in/";

	public static String post_endpoint_test() {

		String post_url_testNg = hostname + "api/users";
		System.out.println(post_url_testNg);
		return post_url_testNg;

	}

	public static String Patch_endpoint_test() {

		String patch_URL_testNG = hostname + "api/users/2";
		System.out.println(patch_URL_testNG);
		return patch_URL_testNG;
	}

	public static String Get_endpoint_test() {

		String get_url_test=hostname+"api/users?page=2";
		System.out.println(get_url_test);
		return get_url_test;
	}

	public static String Put_endpoint_test() {

		String put_url_test= hostname+"api/users/2";
		System.out.println(put_url_test);
		return put_url_test;
	}
	
	public static String Delete_Endpoint_test() {
		 
		String delete_URL_test= hostname+"api/users/2";
		System.out.println(delete_URL_test);
		return delete_URL_test;
	}
	}
