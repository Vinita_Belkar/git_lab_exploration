package request_repository_testNG;

import java.io.IOException;
import java.util.ArrayList;

import commonUtilityPackage_testNG.Excel_data_reader_testNG;

public class Patch_request_repository_testNG extends Endpoints_testNg  {
	
	public static String Patch_request_testNG() throws IOException {
		
		ArrayList<String> excel_data_patch=Excel_data_reader_testNG.Read_Excel_Data("API_TestNG.xlsx", "PatchAPI_TestNG", "Patch_tc2_testNG");
		String req_name= excel_data_patch.get(1);
		String req_job= excel_data_patch.get(2);
		
		String RequestBody="{\r\n"
				+ "    \"name\": \""+ req_name+"\",\r\n"
				+ "    \"job\": \""+req_job+"\"\r\n"
				+ "}";
		System.out.println("Requestbody is:"+RequestBody);
		return RequestBody;
	}

}
