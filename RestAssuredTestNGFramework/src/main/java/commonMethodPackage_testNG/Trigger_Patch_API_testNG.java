package commonMethodPackage_testNG;
import static io.restassured.RestAssured.given;

import request_repository_testNG.Patch_request_repository_testNG;

public class Trigger_Patch_API_testNG extends Patch_request_repository_testNG{
	
	public static int extract_patch_statuscode_testNG(String RequestBody, String patch_URL_testNG) {
		
		int statuscode=given().header("Content-Type", "application/json").body(RequestBody).when().patch(Patch_endpoint_test()).then().extract().response().statusCode();
		System.out.println("statuscode is:"+statuscode);
		return statuscode;
	
	}
	
	public static String extract_patch_response_testNG(String RequestBody, String patch_URL_testNG ) {
		
		String Responsebody=given().header("Content-Type", "application/json").body(RequestBody).when().patch(Patch_endpoint_test()).then().extract().response().asString();
		System.out.println(Responsebody);
		return Responsebody;
		
	}

}

