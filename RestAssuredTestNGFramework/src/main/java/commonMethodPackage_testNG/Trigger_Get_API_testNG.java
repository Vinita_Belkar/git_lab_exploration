package commonMethodPackage_testNG;
import static io.restassured.RestAssured.given;

import request_repository_testNG.Endpoints_testNg;

public class Trigger_Get_API_testNG extends Endpoints_testNg {
	
	public static int extract_get_statuscode( String get_url_test) {
		
		int statuscode=given().when().get(get_url_test).then().extract().response().statusCode();
		System.out.println(statuscode);
		return statuscode;
	}
	public static String extract_get_response_testNG(String get_url_test) {
		
		String Responsebody=given().when().get(get_url_test).then().extract().response().asString();
		System.out.println(Responsebody);
		return Responsebody;
	}
	

}
