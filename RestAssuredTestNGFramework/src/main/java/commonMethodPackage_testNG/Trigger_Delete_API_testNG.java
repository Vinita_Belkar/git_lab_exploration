package commonMethodPackage_testNG;
import static io.restassured.RestAssured.given;
import request_repository_testNG.Endpoints_testNg;

public class Trigger_Delete_API_testNG extends Endpoints_testNg {
	
	public static int extract_delete_statuscode(String delete_URL_test){
		
		int Statuscode=given().when().delete(delete_URL_test).then().extract().response().statusCode();
		System.out.println(Statuscode);
		return Statuscode;
		
	}

}
