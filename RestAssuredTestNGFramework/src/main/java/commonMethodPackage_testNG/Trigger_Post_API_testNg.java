package commonMethodPackage_testNG;

import static io.restassured.RestAssured.given;

import request_repository_testNG.Post_request_repository_testNg;

public class Trigger_Post_API_testNg extends Post_request_repository_testNg {

	public static int Extract_API_post_statuscode_testNG(String requestbody, String post_url_testNg) {

		int statuscode = given().header("Content-Type", "application/json").body(requestbody).when()
				.post(post_url_testNg).then().extract().response().statusCode();
		System.out.println(statuscode);
		
		return statuscode;
	}

	public static String Extract_API_POST_RESPONSE_TESTNG(String requestbody, String post_url_testNg) {

		String Responsebody = given().header("Content-Type", "application/json").body(requestbody).when()
				.post(post_url_testNg).then().extract().response().asString();
		System.out.println(Responsebody);
		return Responsebody;

	}

}
