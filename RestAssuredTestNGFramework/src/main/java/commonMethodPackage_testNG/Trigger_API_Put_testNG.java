package commonMethodPackage_testNG;

import static io.restassured.RestAssured.given;

import request_repository_testNG.Put_request_repository_testNG;

public class Trigger_API_Put_testNG extends Put_request_repository_testNG {

	public static int extract_put_statuscode_testNG(String requestbody, String put_url_test) {

		int statuscode = given().header("Content-Type", "application/json").body(requestbody).when().put(put_url_test)
				.then().extract().response().statusCode();
		System.out.println(statuscode);

		return statuscode;
	}

	public static String extract_put_response_testNG(String requestbody, String put_url_test) {

		String Responsebody = given().header("Content-Type", "application/json").body(requestbody).when()
				.put(put_url_test).then().extract().response().asString();
		System.out.println(Responsebody);
		return Responsebody;
	}
}
