package commonUtilityPackage_testNG;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Handle_API_logs_testNG {
	
	public static File create_log_directory(String directoryname) {
		
		
		//to fetch current project in java will use system.get property to create directory inside it
		String create_log_directory=System.getProperty("user.dir");
		System.out.println( create_log_directory);
		
		//to create directory inside project will give a name to it first
		File log_directory=new File( create_log_directory + "//API_testNg_logs//" + directoryname );
		//deleting older directory first
		Delete_directory(log_directory);
		log_directory.mkdir();
		return log_directory;
		
	}
	public static boolean Delete_directory(File Directory) {

		// here declaring Diredtory.delete outside the if block because variable declare
		// initially it will be false and after all the operation line no.50 will give
		// true value for the same directory.delete
		boolean directorydeleted = Directory.delete();

		// checking if the directory exists or not if it does the it will enter 26th
		// line
		if (Directory.exists()) {

			// here -- .listfiles will give list of files and similar kind object is stored
			// in array data type so its giving list if files so storing it in Files[]
			// if directory is not null then it will enter next line and for each loop will
			// start and iterate till every files get iterated and stores in file and it
			// will check all the file
			File[] files = Directory.listFiles();
			if (files != null) {
				for (File file : files) {

					// if file is directory then Directory.delete will delete it else file will be
					// deleted
					// it will delete all the internal files
					if (file.isDirectory()) {
						Directory.delete();
					} else {
						file.delete();
					}

				}

			}
			// once all the files deleted, it will delete the directory
			directorydeleted = Directory.delete();
		} else {
			System.out.println("Directory does not exists.");
		}
		// return statement is written at the last of method
		return directorydeleted;
	}

	public static void evidence_creator(File directoryname, String filename, String endpoint, String requestBody,
			String responseBody) throws IOException {

		// Step 1:- Create the file at given location
		// (so for file creation will need file name for which we have passed argument
		// of dirname then file name-->name of file)
		// and .txt because the extension of the file is in .txt
		File newfile = new File(directoryname + "\\" + filename + ".txt");
		System.out.println("new file created to save evidence:" + newfile.getName());

		// Step 2:- Write data into the file
		// (so for writting into the file theres another class called FileWriter)
		// This object new FileWriter will need argument of file and file is stored in
		// newfile variable see line 72

		FileWriter datawriter = new FileWriter(newfile);

		// so all the operations of writting deleting can be performed in datawriter
		datawriter.write("End point:" + endpoint + "\n\n");
		datawriter.write("Request Body:\n\n" + requestBody + "\n\n"); // here--> \n denotes next line so \n\n will enter
																		// double

		datawriter.write("Response Body:\n\n" + responseBody);
		// datawriter.close will automatically save and close the file
		datawriter.close();
		System.out.println("Evidence is written in file:" + newfile.getName());

	}

}



